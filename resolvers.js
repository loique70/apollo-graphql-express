const Post = require('./models/Post.models')

const resolvers = {
    Query:{
        hello:() =>{
            return "Hello world Loiqueee"
        },
        getAllPosts: async () =>{
            return await Post.find()  
        },
        getPost:async (parent,{id}) =>{
            return await Post.findById(id)
        }
    },

    Mutation:{
        createPost: async (parent,args,context,info) =>{
            const {title, description} = args.post
            console.log(title,description)
            const post = new Post({title,description})
            await post.save()
            return post
        },

        deletePost: async(parent,{id}) =>{
              await Post.findByIdAndDelete(id)
              return "The post is deleted !!"
        },

        updatePost: async(parent,args,context,info) =>{
            const {title,description} = args.post
            const {id} = args
            const post = await Post.findByIdAndUpdate(id,{title,description}, {new: true})
            return post
        }
    }
}

module.exports = resolvers