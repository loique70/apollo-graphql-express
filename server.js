const express = require('express')
const {gql, ApolloServer} = require('apollo-server-express')
const typeDefs = require('./typeDefs')
const resolvers = require('./resolvers')
const mongoose = require('mongoose')


async function startServer(){
    const server = express()
    const apolloServer = new ApolloServer({
        typeDefs: typeDefs,
        resolvers: resolvers 
    })

    await apolloServer.start()

    apolloServer.applyMiddleware({app: server})

    server.use((req,res) => {
        res.send('Hello from express apollo server')
    })

     await mongoose.connect('mongodb://localhost:27017/post_db',{
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    console.log('Mongoose Connected...')
    server.listen(4000, () => console.log('Server is running on port 4000'))
}
startServer()